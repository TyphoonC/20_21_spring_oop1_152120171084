

#include<stdlib.h>
#include<iostream>
#include<fstream>
#include<vector>
using namespace std;
int numbersSum(vector<int>);
int numberMultiply(vector<int>);
int smallestNumber(vector<int>);
float avarage(vector<int>);
vector <int> readNumver();

/* @brief : bu fonksiyon degiskenleri tanimlar ve fonksiyonları cagirir.*/
int main()
{
	ofstream myfile;
	myfile.open("output.txt");

	vector<int>numbers;
	vector<int>numberRead;
	numberRead = readNumver();
	int size = numberRead[0];

	for (int i = 1; i < size + 1; i++) {
		numbers[i] = numberRead[i];
	}

	int sum = numbersSum(numbers);
	int multiply = numberMultiply(numbers);
	int smallest = smallestNumber(numbers);
	float avg = avarage(numbers);
	myfile << "multiply of numbers: " << multiply << endl;
	myfile << "sum of numbers: " << sum << endl;
	myfile << "smallest of numbers: " << smallest << endl;
	myfile << "avarage of numbers: " << avg << endl;

}

/* @brief : bu fonksiyon dosyadan sayilari alarak bir vektore atar.*/
vector <int> readNumver()
{
	int number;
	vector<int>numberRead;
	ifstream myFile;
	myFile.open("input.txt");

	while (!myFile.eof()) {
		myFile >> number;
		numberRead.push_back(number);
	}
	return numberRead;
}

/* @brief : bu fonksiyon sayilarin toplamini hesaplar.*/
int numbersSum(vector<int>numbers)
{
	int sum = 0;
	int i = 0;
	int size = numbers.size();
	for (i = 0; i < size; i++)
	{
		sum += numbers[i];

	}
	return sum;

}

/* @brief : bu fonksiyon sayilarin carpimini hesaplar.*/
int numberMultiply(vector<int>numbers)
{
	int i = 0, multiply = 1;
	int size = numbers.size();
	for (i = 0; i < size; i++)
	{
		multiply *= numbers[i];
	}
	return multiply;

}

/* @brief : bu fonksiyon sayilarin en kucugunu bulur.*/
int smallestNumber(vector<int>numbers)
{

	int smallest = numbers[0];
	int size = numbers.size();
	int i;
	for (i = 0; i < size; i++)
	{
		if (numbers[i] < smallest)
			smallest = numbers[i];
	}
	return smallest;

}

/* @brief : bu fonksiyon sayilarin ortalamasini bulur.*/
float avarage(vector<int>numbers)
{
	float avg = 0.0;

	for (int i = 0; i < numbers.size(); i++)
	{
		avg += numbers[i];
	}
	int size = numbers.size();
	avg = avg / size;
	return avg;
}